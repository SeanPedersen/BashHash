"""
TODO:
-Stop abusing ProcessPoolExecutor.map for hash cracking, instead create individual child_procs

Insight: ProcessPoolExecutor.map is suited for functions that terminate roughly at the same time (http downloads etc.)
"""

import math
import os
import signal
from itertools import product
from collections import deque
import concurrent.futures
from  concurrent.futures.process import BrokenProcessPool

__author__ = "Sean Pedersen"

def brute_force(hash_fun, hash_nut, alphabet, length, start=0, end=0, prefix="", verbose=0):
    # Brute force an alphabet with given length
    search_space = product(list(alphabet), repeat=length)
    space_size = len(list(alphabet))**length

    if start > space_size:
        raise ValueError("Start parameter exceeds search space size.")

    if verbose == 2: print("Search space size:", space_size)

    counter = 0
    work = False

    for permu in search_space:
        permu = prefix + "".join(permu)
        counter += 1

        if counter >= start:
            work = True
        if work:
            if verbose == 2:
                print("\r" + str(counter) + "/" +
                      str(space_size) + " - " + permu, end="")

            if hash_fun(permu.encode("utf-8"), hash_nut):
                if verbose:
                    print("\nNUT IS CRACKED:", permu)
                return permu
            elif  counter == end:
                break

def gen(iterable, items):
    """ Helper func for itemtee/1 """
    while True:
        if not items:
            items.extend(next(iterable))
        yield items.popleft()

def itemtee(iterable):
    """ Unpacks every tuple of iterable for executor.map to feed to the parallelized function """
    # Assumes shape of all eles in iterable is uniform (TODO: add check with exception)
    return [gen(iter(iterable), deque())] * len(iterable[0])

def multi_brute(hash_fun, hash_nut, alphabet, length, n_procs=2, prefix="", verbose=0):
    """ Partitions search space & parallizes using multi-processing across multiple CPU cores """
    result = None
    parts = []
    SIZE = len(alphabet)**length
    STEP_SIZE = math.ceil(SIZE / n_procs)

    if verbose:
        print("SIZE", SIZE)
        print("STEP_SIZE", STEP_SIZE)

    # Partitioning
    for i in range(0, SIZE, STEP_SIZE):
        part = (hash_fun, hash_nut, alphabet, length, i, i + STEP_SIZE, prefix, verbose)
        if verbose == 2: print(part)
        parts.append(part)

    # Spawn processes
    with concurrent.futures.ProcessPoolExecutor() as executor:
        try:
            # Map spawns one process for each entry in parts, max. parallel procs == CPUCores.count
            for res in executor.map(brute_force, *itemtee(parts)): 
                if res:
                    result = res
                    # Dirty hack to not waste time with the other processes, once the correct hash is found
                    for proc_id in executor._processes:
                        os.kill(proc_id, signal.SIGTERM)
        except BrokenProcessPool:
            # Part of the dirty hack
            pass

    return result
